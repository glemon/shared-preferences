package com.i2g.shared_preferances_app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class edit_pref_Activity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pref_);
        EditText editText1 = (EditText) findViewById(R.id.editText);
        EditText editNumber1 = (EditText) findViewById(R.id.editText2);
        //read from shared preferances
        SharedPreferences sharedPref = edit_pref_Activity.this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        int numDefaultValue = 0;
        int numValue = sharedPref.getInt(getString(R.string.pref_int_value), numDefaultValue);
        String StringDefaultValue = "Google is your friend. Always trust me!";
        String stringValue = sharedPref.getString(getString(R.string.pref_string_value), StringDefaultValue);

        // set fields with init values
        editNumber1.setText(Integer.toString(numValue));
        editText1.setText(stringValue , TextView.BufferType.EDITABLE);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_pref_, menu);
        return true;
    }
    public void saveProperties(View view) {
        // Do something in response to button
        EditText editText1 = (EditText) findViewById(R.id.editText);
        String textVar = editText1.getText().toString();           // textVar has our text variable

        EditText editNumber1 = (EditText) findViewById(R.id.editText2);
        String Num = editNumber1.getText().toString();


        try {
            int numVar = Integer.parseInt(Num);  // num Var Has the number
        }
        catch(Exception e) {
            Log.e("logtag", "Exception: " + e.toString());
        }
        //save data to shared prefrences
        SharedPreferences sharedPref = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        int numVar = Integer.parseInt(Num);  // num Var Has the number
        editor.putInt(getString(R.string.pref_int_value), numVar);
        editor.putString(getString(R.string.pref_string_value), textVar);
        editor.commit();


    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
